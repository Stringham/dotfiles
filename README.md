To use these files:

```
git clone https://gitlab.com/Stringham/dotfiles.git
# set your Windows home path if using wsl
export WINHOME=wslpath $(/c/Windows/System32/cmd.exe /c "<nul set /p=%UserProfile%" 2>/dev/null)
# set up minttyrc
cp mintty/minttyrc $WINHOME/.minttyrc
# set up tmux inside WSL, so use the standard ~ home directory
cp tmux/tmux.conf ~/tmux.conf
# set up the vim folder
cp -a vim ~/.vim
# pull down the submodules
pushd ~/.vim/bundle; for submodule in *; do pushd $submodule; git pull origin master; popd; done; popd
# add a symlink to the vimrc file
ln -s ~/.vim/vimrc ~/.vimrc
```
